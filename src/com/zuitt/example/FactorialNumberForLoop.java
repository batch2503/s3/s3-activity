package com.zuitt.example;

import java.util.Scanner;

public class FactorialNumberForLoop {
    public static void main(String[] args){

        Scanner sc = new Scanner(System.in);
        System.out.println("Input an integer whose factorial will be computed: ");

        try {
            int number = sc.nextInt();

            if (number >= 0) {
                int factorial = 1;
                for (int i = 1; i <= number; i++) {
                    factorial = factorial * i;
                }
                System.out.println("Factorial of " + number + " is: " + factorial);
            }else if(number < 0){
                System.out.println("You inputted negative number, please enter a positive integer!");
            }

        }catch (Exception e){
            System.out.println("Invalid input!");
        }


    }
}
